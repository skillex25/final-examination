using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreManager : MonoBehaviour
{
    public int score;
    public Text scoreDisplay;
    public GameObject level1;
    public GameObject level2;
    public GameObject level3;

    private void Update()
    {
        scoreDisplay.text = score.ToString();
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Obstacle"))
        {
            //increase score of 1!!
            score++;
            Debug.Log(score);

            //bg change color
            if (scoreDisplay.text.Equals("6"))
            {
                level1.SetActive(true);
                level2.SetActive(false);
                level3.SetActive(false);
            }

            if (scoreDisplay.text.Equals("10"))
            {
                level1.SetActive(false);
                level2.SetActive(true);
                level3.SetActive(false);
            }
        }
    }
}